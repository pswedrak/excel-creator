import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

public class UnsafeExcelCreator implements ExcelCreator {
    @Override
    public Workbook createWorkbook(List<Meeting> meetings) {
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("test sheet");
        Row initialRow = sheet.createRow(0);
        initialRow.createCell(0).setCellValue("The client's name");
        initialRow.createCell(1).setCellValue("Time");
        initialRow.createCell(2).setCellValue("Reason");

        int i = 1;
        for(Meeting meeting: meetings){
            Row row = sheet.createRow(i);
            row.createCell(0).setCellValue(meeting.getClientsName());
            row.createCell(1).setCellValue(meeting.getTime());
            row.createCell(2).setCellValue(meeting.getReason());
        }
        return wb;
    }
}
