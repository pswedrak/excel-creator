public class Meeting {
    private String clientsName;
    private String time;
    private String reason;

    public Meeting(String clientsName, String time, String reason){
        this.clientsName = clientsName;
        this.time = time;
        this.reason = reason;
    }

    public String getClientsName() {
        return clientsName;
    }

    public void setClientsName(String clientsName) {
        this.clientsName = clientsName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
