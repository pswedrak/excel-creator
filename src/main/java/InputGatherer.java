import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

class InputGatherer {
    static List<Meeting> createMeetings(){
        System.out.println("Please provide the information about meetings");
        System.out.println("in the format: client name;time;reason for a meeting");
        System.out.println("When you finish please type 'end'");

        List<Meeting> meetings = new ArrayList<Meeting>();
        Scanner scanner = new Scanner(System.in);
        while(Boolean.TRUE){
            String meetingInfo = scanner.nextLine();
            if(meetingInfo.equals("end")){
                break;
            }
            String[] info = meetingInfo.split(";");
            Meeting meeting = new Meeting(info[0], info[1], info[2]);
            meetings.add(meeting);
        }
        return meetings;
    }
}
