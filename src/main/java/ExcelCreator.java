import org.apache.poi.ss.usermodel.Workbook;

import java.util.List;

public interface ExcelCreator {
    Workbook createWorkbook(List<Meeting> meetings);
}
