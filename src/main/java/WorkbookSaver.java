import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class WorkbookSaver {
    public static void saveWorkbook(Workbook workbook, String name){
        try  (OutputStream fileOut = new FileOutputStream(name + ".xls")) {
            workbook.write(fileOut);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
