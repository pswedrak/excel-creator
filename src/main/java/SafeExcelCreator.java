import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.util.List;

public class SafeExcelCreator implements ExcelCreator {
    @Override
    public Workbook createWorkbook(List<Meeting> meetings) {
        Workbook wb = new HSSFWorkbook();
        Sheet sheet = wb.createSheet("test sheet");
        Row initialRow = sheet.createRow(0);
        initialRow.createCell(0).setCellValue("The client's name");
        initialRow.createCell(1).setCellValue("Time");
        initialRow.createCell(2).setCellValue("Reason");
        CellStyle safeCellStyle = wb.createCellStyle();
        safeCellStyle.setQuotePrefixed(Boolean.TRUE);

        int i = 1;
        for (Meeting meeting : meetings) {
            Row row = sheet.createRow(i);
            Cell nameCell = row.createCell(0);
            nameCell.setCellValue(meeting.getClientsName());
            nameCell.setCellStyle(safeCellStyle);

            Cell timeCell = row.createCell(1);
            timeCell.setCellValue(meeting.getTime());
            timeCell.setCellStyle(safeCellStyle);

            Cell reasonCell = row.createCell(2);
            reasonCell.setCellValue(meeting.getReason());
            reasonCell.setCellStyle(safeCellStyle);
        }

        return wb;
    }
}
