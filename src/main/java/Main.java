import org.apache.poi.ss.usermodel.Workbook;
import java.util.List;

public class Main {
    public static void main(String[] args){
        List<Meeting> meetings = InputGatherer.createMeetings();
        Workbook safeWorkbook = new SafeExcelCreator().createWorkbook(meetings);
        Workbook unsafeWorkbook = new UnsafeExcelCreator().createWorkbook(meetings);
        WorkbookSaver.saveWorkbook(safeWorkbook, "safeWorkbook");
        WorkbookSaver.saveWorkbook(unsafeWorkbook, "unsafeWorkbook");
    }
}
